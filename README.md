

# Venv Setup
```bash
python3.8 -m venv .pi
source .pi/bin/activate
python3 -m pip install wheel==0.38.4
python3 -m pip install lit~=15.0
python3 -m pip install --pre torch==2.0.0.dev20230223+cu118 --index-url https://download.pytorch.org/whl/nightly/cu118
python3 -m pip install pytorch-lightning transformers wandb nltk sacrebleu bert-score sacremoses datasets rouge_score evaluate fairscale tensorboard scikit-learn --no-cache-dir
```