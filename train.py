# coding=utf-8

__author__ = "hw56@indiana.edu"
__version__ = "alpha"
__license__ = "ISC"

import os
import json
import torch
import argparse
import datasets
import numpy as np
import pandas as pd
from constants import *
from evaluate import load
from itertools import chain
import pytorch_lightning as pl
from torch.utils.data import Dataset, DataLoader
from pytorch_lightning.loggers import WandbLogger
from transformers import T5TokenizerFast, T5ForConditionalGeneration
from pytorch_lightning.callbacks import (ModelCheckpoint,
                                         LearningRateMonitor,
                                         EarlyStopping,
                                         TQDMProgressBar)


class LyricsInterpretationDataset(Dataset):
    def __init__(
            self,
            data: list,  # a list of dicts of paired texts, keyed by, e.g., `source` `target` `discipline` `title` `doi`
            tokenizer_name: str = BASE_MODEL_NAME,
            max_source_length: int = MAX_SOURCE_LENGTH,
            max_target_length: int = MAX_TARGET_LENGTH,
    ):
        self.data = data
        self.tokenizer = T5TokenizerFast.from_pretrained(tokenizer_name)
        self.max_source_length = max_source_length
        self.max_target_length = max_target_length

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index: int):
        datum = self.data[index]
        source_text = datum['lyrics']
        target_text = datum['comment']
        task_instruction = ''  # todo
        encoding = self.tokenizer(task_instruction + source_text,
                                  max_length=self.max_source_length, padding='max_length', truncation=True,
                                  return_tensors='pt')
        target_encoding = self.tokenizer(target_text,
                                         max_length=self.max_source_length, padding='max_length', truncation=True)
        # replace all tokenizer.pad_token_id in the labels by -100 when we want to ignore padding in the loss
        labels = torch.tensor(
            [(label if label != self.tokenizer.pad_token_id else -100) for label in target_encoding['input_ids']],
            dtype=torch.int64)

        return dict(
            input_ids=encoding['input_ids'].flatten(),
            attention_mask=encoding['attention_mask'].flatten(),
            labels=labels.flatten(),
            target_text=target_text,
            source_text=source_text,
            songmeanings_id=datum['songmeanings_id'],
            music4all_id=datum['music4all_id'])


class LyricsInterpretationDataModule(pl.LightningDataModule):

    def __init__(
            self,
            dataset_version: str = 'non_negative',
            tokenizer: str = BASE_MODEL_NAME,
            batch_size: int = 1,
            max_source_length: int = MAX_SOURCE_LENGTH,
            max_target_length: int = MAX_TARGET_LENGTH,
            num_workers: int = NUM_WORKERS,
            validation_cap: int = NUM_VALIDATION_SAMPLE,
    ):
        super().__init__()
        self.batch_size = batch_size
        self.tokenizer = tokenizer
        self.max_source_length = max_source_length
        self.max_target_length = max_target_length
        self.num_workers = num_workers
        assert dataset_version in ['full', 'non_negative', 'positive'], print(
            f"Dataset version cannot be {dataset_version}, choose one from {['full', 'non_negative', 'positive']}.")
        if dataset_version == 'full':
            self.dataset = json.load(open(FULL_DATASET_PATH, 'r'))
        elif dataset_version == 'non_negative':
            # the dataset has been pushed to hf hub with `dataset.push_to_hub("haining/lyrics_interpretation_non_negative", private=True)`
            # self.dataset = datasets.load_from_disk(LI_DATASET_PATH)
            self.dataset = datasets.load_dataset("haining/lyrics_interpretation_non_negative", use_auth_token=True)
        else:  # positive
            self.dataset = json.load(open(POSITIVE_DATASET_PATH, 'r'))
        self.train_dataset, self.val_dataset, self.test_dataset = None, None, None
        self.validation_cap = validation_cap

    def setup(self, stage=None):
        self.train_dataset = LyricsInterpretationDataset(
            self.dataset['train'], self.tokenizer, self.max_source_length, self.max_target_length)
        if self.validation_cap:
            self.val_dataset = LyricsInterpretationDataset(
                self.dataset['validation'].select(range(self.validation_cap)), self.tokenizer,
                self.max_source_length, self.max_target_length)
        else:
            self.val_dataset = LyricsInterpretationDataset(
                self.dataset['validation'], self.tokenizer,
                self.max_source_length, self.max_target_length)
        self.test_dataset = LyricsInterpretationDataset(
            self.dataset['test'], self.tokenizer, self.max_source_length, self.max_target_length)

    def train_dataloader(self):
        return DataLoader(self.train_dataset, batch_size=self.batch_size, shuffle=True, num_workers=self.num_workers)

    def val_dataloader(self):
        return DataLoader(self.val_dataset, batch_size=self.batch_size, shuffle=False, num_workers=self.num_workers)

    def test_dataloader(self):
        return DataLoader(self.test_dataset, batch_size=self.batch_size, shuffle=False, num_workers=self.num_workers)


class LyricsInterpretationModel(pl.LightningModule):
    """
    An efficient-t5-based translation model built for lyrics interpretation.
    """

    def __init__(self,
                 model_name: str = BASE_MODEL_NAME,
                 log_text: bool = LOG_TEXT,
                 metrics: list = METRICS):
        super().__init__()
        self.model = T5ForConditionalGeneration.from_pretrained(model_name)
        self.tokenizer = T5TokenizerFast.from_pretrained(model_name)
        self.log_text = log_text
        # metrics
        self.metrics = metrics
        self.metric_sacrebleu = load('sacrebleu')
        self.metric_meteor = load('meteor')
        self.metric_bertscore = load('bertscore')
        self.metric_sari = load('sari')
        self.metric_rouge = load('rouge')
        self.metric_bleu = load('bleu')
        self.log_text_container = None
        self.save_hyperparameters()

    def forward(self, input_ids, attention_mask, labels):
        output = self.model(input_ids=input_ids, attention_mask=attention_mask, labels=labels)

        return output.loss, output.logits

    def training_step(self, batch, batch_size):
        input_ids = batch['input_ids']
        attention_mask = batch['attention_mask']
        labels = batch['labels']
        decoder_input_ids = self.model._shift_right(labels)

        loss, logits = self(
            input_ids=input_ids,
            attention_mask=attention_mask,
            labels=labels,
            # use_cache=False
        )
        self.log("train_loss", loss, batch_size=batch_size, sync_dist=True)

        return loss

    def on_validation_start(self):
        # init wandb containers
        if self.log_text and self.global_step % LOG_TEXT_INTERVAL == 0:
            self.log_text_container = pd.DataFrame(columns=['lyrics', 'comment', 'generated_interpretation',
                                                            'songmeanings_id', 'music4all_id'])

    def validation_step(self, batch, batch_size):
        input_ids = batch['input_ids']
        attention_mask = batch['attention_mask']
        labels = batch['labels']

        loss, logits = self(
            input_ids=input_ids,
            attention_mask=attention_mask,
            labels=labels)
        # the output of self() is: odict_keys(['loss', 'logits', 'past_key_values', 'encoder_last_hidden_state'])
        self.log("val_loss", loss, batch_size=batch_size, sync_dist=True)

        # compute metrics only when
        # if self.log_text and self.global_rank == 0 and self.global_step % LOG_TEXT_INTERVAL == 0:
        if self.log_text and self.global_step % LOG_TEXT_INTERVAL == 0:
            self._generate_step(batch, 'val')

        return loss

    def _generate_step(self, batch, data_split: str):
        assert data_split in ['val', 'test']
        # generate given decoding strategy and compute metrics
        source_texts = batch['lyrics']
        target_texts = [[r.strip()] for r in batch['comment']]  # a list of lists
        songmeanings_id = batch['songmeanings_id']
        music4all_id = batch['music4all_id']

        # log decoding
        output_sequences = self.model.generate(
            input_ids=batch["input_ids"],
            attention_mask=batch["attention_mask"],
            max_length=MAX_TARGET_LENGTH,
            top_p=TOP_P_PROB,
            repetition_penalty=REPETITION_PENALTY,
            do_sample=True)
        # compute common metrics: sacrebleu and meteor
        predictions = [p.strip() for p in self.tokenizer.batch_decode(output_sequences, skip_special_tokens=True)]
        metrics = self.calculate_common_metrics(source_texts, predictions, target_texts)
        # logging metrics and texts
        for m in self.metrics:
            if m == 'rouge':
                wandb_logger.log_metrics({f"{data_split}_{str(self.global_step)}/{'rouge1'}": metrics['rouge1']})
                wandb_logger.log_metrics({f"{data_split}_{str(self.global_step)}/{'rouge2'}": metrics['rouge2']})
                wandb_logger.log_metrics({f"{data_split}_{str(self.global_step)}/{'rougeL'}": metrics['rougeL']})
                wandb_logger.log_metrics({f"{data_split}_{str(self.global_step)}/{'rougeLsum'}": metrics['rougeLsum']})
            elif m == 'bertscore':
                wandb_logger.log_metrics(
                    {f"{data_split}_{str(self.global_step)}/{'bertscore_f1'}": metrics['bertscore_f1']})
            else:
                wandb_logger.log_metrics({f"{data_split}_{str(self.global_step)}/{m}": metrics[m]})
        self.log_text_container = pd.DataFrame({
            'lyrics': source_texts,
            'comment': list(chain(*target_texts)),
            'generated_interpretation': predictions,
            'songmeanings_id': songmeanings_id,
            'music4all_id': music4all_id})

    def on_validation_end(self):
        # log texts
        data_split = 'val'
        if self.log_text and self.global_step % LOG_TEXT_INTERVAL == 0:
            wandb_logger.log_text(
                key=f"{data_split}_{self.global_step}",
                dataframe=self.log_text_container.loc[:NUM_LOG_TEXT_ROWS - 1, :])

    def test_step(self, batch, batch_size):
        input_ids = batch['text_input_ids']
        attention_mask = batch['text_attention_mask']
        labels = batch['labels']

        loss, outputs = self(
            input_ids=input_ids,
            attention_mask=attention_mask,
            labels=labels
        )
        # the output of self() is: odict_keys(['loss', 'logits', 'past_key_values', 'encoder_last_hidden_state'])
        self.log("test_loss", loss, batch_size=batch_size, sync_dist=True)
        if self.log_text:
            self._generate_step(batch, 'test')

        return loss

    def configure_optimizers(self):
        return torch.optim.AdamW(self.trainer.model.parameters(), lr=LR)

    def calculate_common_metrics(self,
                                 source_texts: list,
                                 predictions: list,
                                 target_texts: list):
        """
        Computes common metrics.
        Args:
            source_texts: list of str, source texts
            predictions: list of str, generated texts
            target_texts: list of list, by convention found in the datasets library

        Returns:
            dict, keyed by `metrics`
        """
        metrics_dict = {}
        predictions = [p.strip() for p in predictions]
        target_texts = [[t_.strip() for t_ in t] for t in target_texts]
        source_texts = [r.strip() for r in source_texts]
        for metric in self.metrics:
            if metric == 'sacrebleu':
                metrics_dict.update({'sacrebleu':
                    np.mean(
                        self.metric_sacrebleu.compute(predictions=predictions, references=target_texts)[
                            'score'])})
            elif metric == 'meteor':
                metrics_dict.update({'meteor': np.mean(
                    self.metric_meteor.compute(predictions=predictions, references=target_texts)['meteor'])})
            elif metric == 'bertscore':
                _bertscore = self.metric_bertscore.compute(predictions=predictions, references=target_texts, lang='en')
                metrics_dict.update({'bertscore_f1': np.mean(_bertscore['f1'])})
                # metrics_dict.update({'bertscore_precision': np.mean(_bertscore['precision'])})
                # metrics_dict.update({'bertscore_recall': np.mean(_bertscore['recall'])})
            elif metric == 'sari':
                metrics_dict.update(
                    {'sari': np.mean(self.metric_sari.compute(sources=source_texts,
                                                              predictions=predictions,
                                                              references=target_texts)['sari'])})
            elif metric == 'rouge':
                _rouge = self.metric_rouge.compute(predictions=predictions, references=target_texts)
                metrics_dict.update({'rouge1': np.mean(_rouge['rouge1'])})
                metrics_dict.update({'rouge2': np.mean(_rouge['rouge2'])})
                metrics_dict.update({'rougeL': np.mean(_rouge['rougeL'])})
                metrics_dict.update({'rougeLsum': np.mean(_rouge['rougeLsum'])})
            elif metric == 'bleu':
                metrics_dict.update({'bleu': self.metric_bleu.compute(predictions=predictions,
                                                                      references=target_texts)['bleu']})

        return metrics_dict


if __name__ == '__main__':

    pl.seed_everything(0)

    # wandb environment variable
    os.environ["WANDB_CACHE_DIR"] = "wandb"


    def precision_types(precision):
        if isinstance(precision, int):
            return precision
        if precision == 'bf16':
            return precision
        if precision in ['16', '32', '64']:
            return int(precision)
        else:
            raise argparse.ArgumentTypeError('Str value expected.')

    parser = argparse.ArgumentParser(description="Interpreting the meaning of lyrics.")
    parser.add_argument("-v", "--version", action="version",
                        version="%(prog)s (version {version})".format(version=__version__), )
    parser.add_argument("--project_name", default='lyrics_interpretation', required=True)
    parser.add_argument("--run_name", default='baseline', required=True)
    parser.add_argument(
        "--dataset_version", default="non_negative", choices=['full', 'non_negative', 'positive'], help="")
    parser.add_argument("--base_model_name", default='google/flan-t5-large')
    parser.add_argument("--per_gpu_batch_size", default=1, type=int)
    parser.add_argument("--learning_rate", default=LR, type=float)
    parser.add_argument("--max_source_length", default=MAX_SOURCE_LENGTH, type=int)
    parser.add_argument("--max_target_length", default=MAX_TARGET_LENGTH, type=int)
    parser.add_argument("--num_workers", default=NUM_WORKERS, type=int)
    parser.add_argument("--num_nodes", default=1, type=int, help="Number of nodes.")
    parser.add_argument("--devices", default=2, type=int, help="Number of GPUs available per node.")
    parser.add_argument("--precision", default='bf16', type=precision_types, help="Precision, supprots Double precision (64), full precision (32), half precision (16) or bfloat16 precision (bf16).")
    parser.add_argument("--strategy", default='FSDP', help="Fully Sharded Data Parallel by default.")
    parser.add_argument("--max_epochs", default=200, type=int, help="")
    parser.add_argument("--log_every_n_steps", default=1000, type=int)
    parser.add_argument("--accumulate_grad_batches", default=5, type=int)
    parser.add_argument("--val_check_interval", default=0.1, type=float)
    parser.add_argument("--validation_cap", default=1200, type=int)
    parser.add_argument("--ckpt_every_n_train_steps", default=1000, type=int)
    parser.add_argument("--ckpt_save_top_k", default=3, type=int)
    parser.add_argument("--patience", default=5, type=int)
    parser.add_argument("--log_text_during_training", dest='log_text_during_training', action='store_true')
    parser.add_argument("--no-log_text_during_training", dest='log_text_during_training', action='store_false')
    parser.set_defaults(log_text_during_training=False)
    parser.add_argument("--resume_from_ckpt_path", default='', type=str)
    parser.add_argument("--resume_wandb_run_id", default='', type=str)
    parser.add_argument('--metrics', nargs='*', default='',
                        choices=METRICS, help='Metrics other than loss to minotor during training.')

    args = parser.parse_args()
    ckpt_dir = f"ckpts/{args.run_name}"

    # init logger
    wandb_logger = WandbLogger(name=args.run_name,
                               project=args.project_name,
                               save_dir='wandb',
                               id=None if not args.resume_wandb_run_id else args.resume_wandb_run_id)


    # prepare the data module
    dm = LyricsInterpretationDataModule(
        dataset_version=args.dataset_version,
        tokenizer=args.base_model_name,
        batch_size=args.per_gpu_batch_size,
        max_source_length=args.max_source_length,
        max_target_length=args.max_target_length,
        num_workers=args.num_workers,
        validation_cap=args.validation_cap)

    # prepare the model
    if args.resume_from_ckpt_path:
        model = LyricsInterpretationModel.load_from_checkpoint(checkpoint_path=args.resume_from_ckpt_path)
    else:
        model = LyricsInterpretationModel(model_name=args.base_model_name,
                                          log_text=args.log_text_during_training,
                                          metrics=args.metrics)  # todo

    # prepare the trainer
    trainer = pl.Trainer(
        # fast prototype
        # fast_dev_run=500,
        # distributed training
        accelerator='gpu',
        devices=args.devices,
        strategy=args.strategy,
        precision=args.precision,
        max_epochs=args.max_epochs,
        accumulate_grad_batches=args.accumulate_grad_batches,
        log_every_n_steps=args.log_every_n_steps,
        val_check_interval=args.val_check_interval,
        callbacks=[ModelCheckpoint(
            dirpath=ckpt_dir,
            every_n_train_steps=args.ckpt_every_n_train_steps,
            filename='{epoch}-{step}-{val_loss:.3f}',
            save_top_k=args.ckpt_save_top_k, verbose=True, monitor='val_loss', mode='min'),
            LearningRateMonitor("step"),
            EarlyStopping(monitor="val_loss", min_delta=0, patience=args.patience, verbose=False, mode="min"),
            TQDMProgressBar(refresh_rate=5)],
        # misc
        enable_progress_bar=True,
        logger=wandb_logger,
        default_root_dir=ckpt_dir,
    )

    trainer.fit(model=model, datamodule=dm)
