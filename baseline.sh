#!/bin/bash

# load venv
source .li/bin/activate

# set huggingface cache
export HF_HOME=~/.cache/huggingface
export WANDB_CACHE_DIR=~/lyrics_interpretation/wandb

# make ckpt and log directory if they do not exist
mkdir -p logs
mkdir -p wandb
mkdir -p ckpts

# main task
nohup python -u train.py \
--project_name=lyrics_interpretation \
--run_name=flanT5Base_on_audio \
--base_model_name=google/flan-t5-base \
--per_gpu_batch_size=1 \
--learning_rate=1e-4 \
--max_source_length=512 \
--max_target_length=384 \
--num_workers=4 \
--num_nodes=1 \
--devices=4 \
--strategy=FSDP \
--max_epochs=200 \
--log_every_n_steps=1000 \
--accumulate_grad_batches=5 \
--val_check_interval=.2 \
--validation_cap=1200 \
--ckpt_every_n_train_steps=1000 \
--ckpt_save_top_k=3 \
--patience=20 \
--no-log_text_during_training >logs/base-baseline.log 2>&1 &
