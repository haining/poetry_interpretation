# coding=utf-8

__author__ = "hw56@indiana.edu"
__version__ = "alpha"
__license__ = "ISC"
# 
# BASE_MODEL_NAME = 'google/t5-efficient-base-nl24'
BASE_MODEL_NAME = 'google/flan-t5-large'
# BASE_MODEL_NAME = 'google/flan-t5-base'
# MAX_SOURCE_LENGTH = 512
MAX_SOURCE_LENGTH = 512
MAX_TARGET_LENGTH = 384
NUM_WORKERS = 8
# the original val indices for 29484 samples are not provided
# so we randomly split that number of samples as val
NUM_VALIDATION_SAMPLE = 1200
ORIGINAL_NUM_VALIDATION_SAMPLE = 29484
LI_DATASET_PATH = 'resource/lyrics_interpretation_hf_dataset'

# whether to resume training with a ckpt
RESUME_FROM_CKPT_PATH = ''

# to log generated texts
NUM_LOG_TEXT_ROWS = 100
LOG_TEXT_INTERVAL = 1e4
TOP_P_PROB = .9
REPETITION_PENALTY = 1.0
LOG_TEXT = False
METRICS = ['sacrebleu', 'meteor', 'bertscore', 'sari', 'rouge', 'bleu']
# LR = 3e-5
LR = 1e-4

# datasets
FULL_DATASET_PATH = 'resource/lyrics_interpretation_dataset/dataset_full_256_clean.json'
# we will use non_negative version since it performs the best from previous work
NON_NEGATIVE_DATASET_PATH = 'resource/lyrics_interpretation_dataset/dataset_not_negative_256_clean.json'
POSITIVE_DATASET_PATH = 'resource/lyrics_interpretation_dataset/dataset_positive_256_clean.json.json'
TEST_DATASET_PATH = 'resource/lyrics_interpretation_dataset/dataset_test.json'

# checkpoints
CKPT_BASELINE = ''
# CKPT_DEMO = CKPT_BASELINE

# result ckpts
