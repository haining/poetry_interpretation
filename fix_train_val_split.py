# coding=utf-8

__author__ = "hw56@indiana.edu"
__version__ = "1.0.0"
__license__ = "ISC"

"""
Fix the data split for the Lyrics Interpretation dataset (non-negative version). 
"""

import json
import datasets
from constants import *
from sklearn.model_selection import train_test_split

train_file_path = 'resource/jsonl/train_dataset_not_negative_256_clean.jsonl'
val_file_path = 'resource/jsonl/val_dataset_not_negative_256_clean.jsonl'
test_file_path = 'resource/jsonl/test_dataset_not_negative_256_clean.jsonl'


def list2jsonl(data_dict_list, path):
    with open(path, 'w') as f:
        for line in data_dict_list:
            json.dump(line, f)
            f.write('\n')


train_val_dataset = json.load(open(NON_NEGATIVE_DATASET_PATH, 'r'))
train_dataset, val_dataset = train_test_split(train_val_dataset, test_size=ORIGINAL_NUM_VALIDATION_SAMPLE,
                                              random_state=42)
test_dataset = json.load(open(TEST_DATASET_PATH, 'r'))

list2jsonl(train_dataset, train_file_path)
list2jsonl(val_dataset, val_file_path)
list2jsonl(test_dataset, test_file_path)

new_dataset = datasets.load_dataset('json', data_files={'train': train_file_path,
                                                        'validation': val_file_path,
                                                        'test': test_file_path})

new_dataset.save_to_disk(LI_DATASET_PATH)
